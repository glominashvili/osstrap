#!/bin/dash
#Author: Goga Lominashvili(g.lominashvili@gmail.com)
#License: MIT

username="svireli"
user_email="g.lominashvili@gmail.com"
pkglist_url="https://gitlab.com/svireli/osstrap/raw/master/headless.csv"
dotfiles_repo="https://gitlab.com/svireli/dotfiles.git"

svc_init() {
	for service in "$@"; do
		echo "Enabling service: $service"
		sudo ln -s "/etc/sv/$service" /var/service
	done;
}

git_install() {
	echo "Installing from Repo: $1"
	repo_name=$(basename "$1")
	repo_dir="/home/$username/projects/$repo_name"
	echo $repo_dir
	sudo -u "$username" mkdir -p "$repo_dir"
	sudo -u "$username" git clone --depth 1 "$1" "$repo_dir"
	cd "$repo_dir"
	sudo -u "$username" make && sudo make install
	cd "/home/$username"
}

xbps_install_csv() {
	curl -L -o /tmp/pkg_list.csv "$pkglist_url"
	count=$(wc -l < /tmp/pkg_list.csv)
	while IFS=, read -r tag program comment; do
		n=$((n+1))
		echo "Installing... $n / $count"
		echo $program $comment
		case "$tag" in
			"") sudo xbps-install -y "$program";;
			"G") git_install "$program";;
		esac
	done < /tmp/pkg_list.csv
}

get_dots() {
	echo "Downloading dotfiles"
	sudo -u "$username" git clone -b "master" --depth 1 "$dotfiles_repo" "/home/$username/projects/dotfiles"
	sudo -u "$username" cp -rfT "/home/$username/projects/dotfiles" "/home/$username"
	sudo cp "/home/$username/projects/dotfiles/xorg.conf" /etc/X11/
	cd "/home/$username/.local/bin" && chmod +x *
	cd "/home/$username"
	rm -rf README.md LICENSE xorg.conf .git
}

echo "Welcome to OSstrap!"
echo "Installation Info"
echo "User: $username"
echo "Packages: $pkglist_url"
echo "Dotfiles: $dotfiles_repo"

echo "Installing prerequisites: base-devel, git, curl and gaining sudo access"
sudo -i xbps-install -Sy base-devel git curl

if [ $? -ne 0 ]; then exit; fi

xbps_install_csv

get_dots

echo "Configuring virtualization and cgroups for non root user"
sudo gpasswd -a "$username" libvirt
sudo usermod -a -G libvirt "$username"

sudo cgm create all "$username"
sudo cgm chown all "$username" $(id -u $username) $(id -g $username)
sudo cgm movepid all "$username" $$

echo "Enabling services"
svc_init dbus cgmanager polkit chronyd NetworkManager alsa udevd libvirtd virtlockd virtlogd
echo "Configuring git user: $username, email: $user_email"
git config --global user.email "$user_email"
git config --global user.name "$username"

echo "Install NVM for node"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash

echo "Installation finished successfully"

# OSstrap

A simple script for configuring a freshly installed Void Linux(created and tested on thinkpad x260).  
Before running make sure your system is up to date and change username and email in the script.

`curl -LO https://gitlab.com/svireli/osstrap/raw/master/osstrap.sh`

edit the script and run

`chmod +x osstrap.sh`  
`./osstrap.sh`

please configure xorg.conf, zzz.d and acpi handlers for your need after the installation.

Inspired by [LARBS](https://larbs.xyz/)